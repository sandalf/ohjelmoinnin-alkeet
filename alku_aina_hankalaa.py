def pyyda_syote(input_str, error_str):
    """
    Kysyy käyttäjältä kokonaislukua käyttäen kysymyksenä parametrina annettua
    merkkijonoa. Virheellisen syötteen kohdalla käyttäjälle näytetään toisena
    parametrina annettu virheilmoitus. Käyttäjän antama kelvollinen syöte
    palautetaan kokonaislukuna. Hyväksyy vain luvut jotka ovat suurempia kuin 1.
    """
    while True:
        try:
            input_luku = int(input(input_str))
        except ValueError:
            print(error_str)
        else:
            if input_luku > 1:
                return input_luku
            else:
                print(error_str)

def tarkista_alkuluku(luku):
    """
    Tarkistaa onko parametrina annettu luku alkuluku. Palauttaa False jos luku ei
    ole alkuluku; jos luku on alkuluku palautetaan True
    """
    for numero in range(2, luku):
        if (luku % numero) == 0:
            return False
    return True

input_numero = pyyda_syote("Anna kokonaisluku, joka on suurempi kuin 1: ", "Pieleen meni :'(.")
if tarkista_alkuluku(input_numero):
    print("Kyseessä on alkuluku.")
else:
    print("Kyseessä ei ole alkuluku.")
