import ikkunasto
import piiristo

tila = {
    "syote": None,
    "laatikko": None,
    "piiri": None,
    "komponentit": [],
    "jannite": 0,
    "taajuus": 0,
}

def muunna_liukuluvuksi(luku, virheviesti):
    try:
        luku = float(luku)
    except ValueError:
        ikkunasto.avaa_viesti_ikkuna("Virhe", virheviesti, True)
        ikkunasto.tyhjaa_kentan_sisalto(tila["syote"])
        return None, False
    return luku, True

def aseta_jannite():
    """
    Lukee liukulukuarvon syötekentästä ja asettaa sen piirin jännitelähteen
    jännitteeksi.
    """
    syote_jannite = ikkunasto.lue_kentan_sisalto(tila["syote"])
    syote_jannite, ok_j = muunna_liukuluvuksi(syote_jannite,
                                              "Syöte ei ollut kelvollinen jännitearvo")
    if ok_j:
        tila["jannite"] = syote_jannite
        ikkunasto.kirjoita_tekstilaatikkoon(tila["laatikko"],
                                                 "Jännite {:.1f} V".format(syote_jannite))
        ikkunasto.tyhjaa_kentan_sisalto(tila["syote"])


def aseta_taajuus():
    """
    Lukee liukulukuarvon syötekentästä ja asettaa sen piirin jännitelähteen
    taajuudeksi.
    """
    syote_taajuus = ikkunasto.lue_kentan_sisalto(tila["syote"])
    syote_taajuus, ok_t = muunna_liukuluvuksi(syote_taajuus,
                                              "Syöte ei ollut kelvollinen taajuusarvo")
    if ok_t:
        tila["taajuus"] = syote_taajuus
        ikkunasto.kirjoita_tekstilaatikkoon(tila["laatikko"],
                                                 "Taajuus {:.1f} Hz".format(syote_taajuus))
        ikkunasto.tyhjaa_kentan_sisalto(tila["syote"])

def lisaa_vastus():
    """
    Lukee liukulukuarvon syötekentästä, ja lisää vastuksen, jonka resistanssi on
    luettu arvo, piirissä olevaan ainoaan haaraan. Päivittää piirikaaviokuvan.
    """
    syote_vastus = ikkunasto.lue_kentan_sisalto(tila["syote"])
    syote_vastus, ok_v = muunna_liukuluvuksi(syote_vastus,
                                             "Syöte ei ollut kelvollinen vastusarvo")
    if ok_v:
        tila["komponentit"].append(("r", syote_vastus))
        piiristo.piirra_jannitelahde(tila["piiri"], tila["jannite"], tila["taajuus"])
        piiristo.piirra_haara(tila["piiri"], tila["komponentit"], 6, 2, viimeinen=True)
        piiristo.piirra_piiri(tila["piiri"])
        ikkunasto.tyhjaa_kentan_sisalto(tila["syote"])


def main():
    """
    Lue käyttöliittymäikkunan, jossa on vasemmalla puolella syötekenttä
    numeroarvoille, neljä nappia ja tekstilaatikko. Oikealla puolella on
    piirikaaviokuva.
    """
    ikkuna = ikkunasto.luo_ikkuna("Nalle")
    kehys = ikkunasto.luo_kehys(ikkuna)
    ikkunasto.luo_tekstirivi(kehys, "arvo:")
    tila["syote"] = ikkunasto.luo_tekstikentta(kehys)
    ikkunasto.luo_nappi(kehys, "aseta jännite", aseta_jannite)
    ikkunasto.luo_nappi(kehys, "aseta taajuus", aseta_taajuus)
    ikkunasto.luo_nappi(kehys, "lisää vastus", lisaa_vastus)
    ikkunasto.luo_nappi(kehys, "quit", ikkunasto.lopeta)
    tila["laatikko"] = ikkunasto.luo_tekstilaatikko(kehys)
    kehys = ikkunasto.luo_kehys(ikkuna)
    tila["piiri"] = piiristo.luo_piiri(kehys, 600, 600, 14)
    ikkunasto.kaynnista()

if __name__ == "__main__":
    main()
