""""
Määrittelee aasigotchin käyttöliittymän toiminnot.
"""
import aasimaaritelmat as am

def nayta_tila(aasin_tila):
    """
    Tulostaa aasin tilan.
    """
    print("Aasi on {} vuotta vanha ja rahaa on {} mk.".format(aasin_tila["IKÄ"],
                                                              aasin_tila["RAHA"]))
    print("Kylläisyys: {}".format(aasin_tila["KYLLÄISYYS"]))
    print("Onnellisuus: {}".format(aasin_tila["ONNELLISUUS"]))
    print("Jaksaminen: {}".format(aasin_tila["JAKSAMINEN"]))
    if aasin_tila["ELÄKE"]:
        print("Aasi on jäänyt eläkkeelle.")


def pyyda_syote(aasin_tila):
    """
    Näyttää käyttäjälle aasin tilaa vastaavat mahdolliset syötteet ja kysyy uutta
    syötettä kunnes käyttäjä antaa laillisen syötteen. Saatu syöte palautetaan.
    """
    if aasin_tila["ELÄKE"]:
        valintalista = am.ELAKEVALINNAT
    else:
        valintalista = am.VALINNAT
    print("Valinnat: " + ", ".join(valintalista))
    while True:
        valinta = input("Anna seuraava valinta: ").lower()
        try:
            valintalista.index(valinta)
        except ValueError:
            print("Virheellinen syöte!")
        else:
            return valinta
