def laske_parametrit(x1, y1, x2, y2):
    kkerroin = (y2 - y1) / (x2 - x1)
    vtermi = (x2 * y1 - x1 * y2) / (x2 - x1)
    return kkerroin, vtermi

input_x1 = float(input("Anna ensimmäisen pisteen x-koordinaatti: "))
input_y1 = float(input("Anna ensimmäisen pisteen y-koordinaatti: "))
input_x2 = float(input("Anna toisen pisteen x-koordinaatti: "))
input_y2 = float(input("Anna toisen pisteen y-koordinaatti: "))
try:
    kulmakerroin, vakiotermi = laske_parametrit(input_x1, input_y1, input_x2, input_y2)
except ZeroDivisionError:
    if input_x1 == input_x2 and input_y1 == input_y2:
        print("Nämähän ovat yksi ja sama piste!")
    else:
        print("Suora on pystysuora, yhtälöä ei voida laskea.")
else:
    if vakiotermi == 0:
        print("Suoran yhtälö: y = {k:.3f}x".format(k=kulmakerroin))
    elif vakiotermi > 0:
        print("Suoran yhtälö: y = {k:.3f}x + {b:.3f}".format(k=kulmakerroin, b=vakiotermi))
    else:
        print("Suoran yhtälö: y = {k:.3f}x - {b:.3f}".format(k=kulmakerroin, b=abs(vakiotermi)))
