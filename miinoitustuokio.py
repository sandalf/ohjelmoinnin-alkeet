import random
import haravasto

tila = {
    "kentta": None
}

def miinoita(kentta_lista, vapaat_ruudut, miina_lkm):
    """
    Asettaa kentällä N kpl miinoja satunnaisiin paikkoihin.
    """
    pituus = len(vapaat_ruudut) - 1
    for num in range(miina_lkm):
        rand = random.randint(0, pituus - num)
        ruutu = vapaat_ruudut[rand]
        print(ruutu)
        kentta_lista[ruutu[1]][ruutu[0]] = "x"
        vapaat_ruudut.remove(ruutu)

def piirra_kentta():
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    haravasto.tyhjaa_ikkuna()
    haravasto.piirra_tausta()
    haravasto.aloita_ruutujen_piirto()
    for i, row in enumerate(tila["kentta"]):
        for j, avain in enumerate(row):
            haravasto.lisaa_piirrettava_ruutu(avain, j * 40, (i + 1) * 40)
    haravasto.piirra_ruudut()

def main():
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """

    haravasto.lataa_kuvat("spritet")
    haravasto.luo_ikkuna(400, 400)
    haravasto.aseta_piirto_kasittelija(piirra_kentta)
    haravasto.aloita()

if __name__ == "__main__":
    kentta = []
    for rivi in range(10):
        kentta.append([])
        for sarake in range(10):
            kentta[-1].append(" ")

    tila["kentta"] = kentta
    jaljella = []
    for x in range(10):
        for y in range(10):
            jaljella.append((x, y))

    miinoita(kentta, jaljella, 25)
    main()
