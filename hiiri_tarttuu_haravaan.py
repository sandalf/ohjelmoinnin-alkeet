import haravasto

HIIREN_PAINIKKEET = {
    haravasto.HIIRI_VASEN: "vasen",
    haravasto.HIIRI_KESKI: "keski",
    haravasto.HIIRI_OIKEA: "oikea"
}

def kasittele_hiiri(x, y, painike, modifier):
    """
    Tätä funktiota kutsutaan kun käyttäjä klikkaa sovellusikkunaa hiirellä.
    Tulostaa hiiren sijainnin sekä painetun napin terminaaliin.
    """
    print("Hiiren nappia {} painettiin kohdassa {}, {}".format(HIIREN_PAINIKKEET[painike], x, y))

def main():
    """
    Luo sovellusikkunan ja asettaa käsittelijäfunktion hiiren klikkauksille.
    Käynnistää sovelluksen.
    """
    haravasto.luo_ikkuna(900, 600)
    haravasto.aseta_hiiri_kasittelija(kasittele_hiiri)
    haravasto.aloita()

if __name__ == "__main__":
    main()
