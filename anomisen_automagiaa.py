def pyyda_syote(input_str, error_str):
    """
    Kysyy käyttäjältä kokonaislukua käyttäen kysymyksenä parametrina annettua
    merkkijonoa. Virheellisen syötteen kohdalla käyttäjälle näytetään toisena
    parametrina annettu virheilmoitus. Käyttäjän antama kelvollinen syöte
    palautetaan kokonaislukuna.
    """
    while True:
        try:
            input_luku = int(input(input_str))
        except ValueError:
            print(error_str)
        else:
            return input_luku
