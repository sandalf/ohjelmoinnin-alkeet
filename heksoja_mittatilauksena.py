def muotoile_heksaluvuksi(luku, bitit):
    hexa = hex(luku)[2:].zfill(bitit // 4)
    return hexa

try:
    input_luku = int(input("Anna kokonaisluku: "))
    input_bitit = int(input("Anna heksaluvun pituus: "))
except ValueError:
    print("Kokonaisluku kiitos")
else:
    hexaluku = muotoile_heksaluvuksi(input_luku, input_bitit)
    print(hexaluku)
