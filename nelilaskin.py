import operator as ops

operaatiot = {
    "+": ops.add,
    "-": ops.sub,
    "*": ops.mul,
    "/": ops.truediv
}

operaatio = input("Valitse operaatio (+, -, *, /): ")
if not operaatiot.get(operaatio, False):
    print("Operaatiota ei ole olemassa")
else:
    try:
        input_luku_1 = float(input("Anna luku 1: "))
        input_luku_2 = float(input("Anna luku 2: "))
    except ValueError:
        print("Ei tämä ole mikään luku")
    else:
        if operaatio == "/" and input_luku_2 == 0:
            print("Tällä ohjelmalla ei pääse äärettömyyteen")
        else:
            print("Tulos: {}".
                  format(operaatiot[operaatio](input_luku_1, input_luku_2)))
