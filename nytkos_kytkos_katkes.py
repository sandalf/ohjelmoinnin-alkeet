def kysy_arvot():
    """
    Pyytää käyttäjältä liukulukuarvoja kunnes käyttäjä syöttää tyhjän. Syötetyt
    arvot palautetaan listana.
    """
    vastukset = []
    while True:
        vastus = input("Anna vastusarvo: ")
        if not vastus:
            return vastukset
        try:
            vastus = float(vastus)
        except ValueError:
            print("Komponentin arvon on oltava nollaa suurempi luku.")
        else:
            if vastus == 0:
                print("Komponentin arvon on oltava nollaa suurempi luku.")
            else:
                vastukset.append(vastus)


def laske_sarja(vastukset_sarjan):
    """
    Laskee annettujen vastusten kokonaisresistanssin kun ne oletetaan sarjaan
    kytketyiksi.
    """
    R_sarjan = 0
    for numero in vastukset_sarjan:
        R_sarjan += numero
    return R_sarjan

def laske_rinnan(vastukset_rinnan):
    """
    Laskee annettujen vastusten kokonaisresistanssin kun ne oletetaan rinnankytketyiksi.
    """
    R_rinnan = 0
    for numero in vastukset_rinnan:
        R_rinnan += 1/numero
    return 1/R_rinnan

vastusarvot = kysy_arvot()
if vastusarvot:
    print("Sarjaresistanssi: {}".format(laske_sarja(vastusarvot)))
    print("Rinnakkaisresistanssi: {}".format(laske_rinnan(vastusarvot)))
else:
    print("Et antanut yhtään vastusta")
