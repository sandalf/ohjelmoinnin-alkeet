import math
import cmath
import ikkunasto

elementit = {
    "tekstilaatikko": None,
    "tekstikentta": None
}

def muuta_osoitinmuotoon():
    """
    Lukee kompleksiluvun syötekentästä ja muuttaa sen osoitinmuotoon, jossa
    osoittimen kulma on esitetty asteina. Kompleksiluku sekä sen osoitinmuoto
    tulostetaan käyttöliittymässä olevaan tekstilaatikkoon.
    """
    syote = ikkunasto.lue_kentan_sisalto(elementit["tekstikentta"]).replace(" ", "")
    try:
        syote = complex(syote)
    except ValueError:
        ikkunasto.avaa_viesti_ikkuna("Virhe", "Syöte ei ollut kelvollinen kompleksiluku", True)
        ikkunasto.tyhjaa_kentan_sisalto(elementit["tekstikentta"])
    else:
        osoitin, kulma = cmath.polar(syote)
        text = "{:.3f} on osoitinmuodossa {:.3f} < {:.3f}°".format(
                                                            syote, osoitin, math.degrees(kulma))
        ikkunasto.kirjoita_tekstilaatikkoon(elementit["tekstilaatikko"], text)
        ikkunasto.tyhjaa_kentan_sisalto(elementit["tekstikentta"])

def main():
    """
    Luo käyttöliittymäikkunan, jossa on vasemmalla tekstikenttä otsikoineen,
    kaksi nappia (muunnosnappi ja quit) ja oikealla tekstilaatikko.
    """
    ikkuna = ikkunasto.luo_ikkuna("Nalle")
    kehys = ikkunasto.luo_kehys(ikkuna)
    ikkunasto.luo_tekstirivi(kehys, "kompleksiluku:")
    elementit["tekstikentta"] = ikkunasto.luo_tekstikentta(kehys)
    ikkunasto.luo_nappi(kehys, "MUUNNA", muuta_osoitinmuotoon)
    ikkunasto.luo_nappi(kehys, "LOPETA", ikkunasto.lopeta)
    kehys = ikkunasto.luo_kehys(ikkuna)
    elementit["tekstilaatikko"] = ikkunasto.luo_tekstilaatikko(kehys)
    ikkunasto.kaynnista()

if __name__ == "__main__":
    main()
