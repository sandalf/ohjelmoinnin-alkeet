import math

def laske_kuvion_ala(x_pituus):
    sade = laske_sivun_pituus(x_pituus)
    nelion_ala = laske_nelion_ala(x_pituus)
    kolmion_ala = laske_nelion_ala(sade) / 2
    ympyran_ala = laske_sektorin_ala(sade, 45)
    nelion_ala_2 = laske_nelion_ala(sade * 2)
    ympyran_ala_2 = laske_sektorin_ala(sade * 2, 270)
    return nelion_ala + kolmion_ala + ympyran_ala + nelion_ala_2 + ympyran_ala_2

def laske_nelion_ala(sivu_pituus):
    pinta_ala = sivu_pituus ** 2
    return pinta_ala

def laske_sektorin_ala(sade, kulma):
    sektorin_pinta_ala = kulma / 360 * math.pi * sade ** 2
    return sektorin_pinta_ala

def laske_sivun_pituus(hypotenuusa):
    sivun_pituus = math.sqrt(hypotenuusa ** 2 / 2)
    return sivun_pituus

input_x = float(input("Anna x: "))
kuvion_ala = laske_kuvion_ala(input_x)
print("Kuvion ala:", round(kuvion_ala, 4))
