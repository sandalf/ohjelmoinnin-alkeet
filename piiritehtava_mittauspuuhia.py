import math
import cmath

def laske_osoitinmuoto(tyyppi, arvo, taajuus=0):
    if tyyppi == "l":
        z_l = 2 * math.pi * taajuus * arvo * 1j
        return cmath.polar(z_l)
    elif tyyppi == "c":
        z_c = 1 / (2 * math.pi * taajuus * arvo * 1j)
        return cmath.polar(z_c)
    return cmath.polar(arvo)

input_tyyppi = input("Anna komponentin tyyppi (R, L, C): ").lower()
if input_tyyppi != "r" and input_tyyppi != "l" and input_tyyppi != "c":
    print("Sallittuja komponentteja ovat R, L ja C!")
else:
    input_arvo = float(input("Anna komponentin arvo: "))
    if input_tyyppi != "r":
        input_taajuus = float(input("Anna komponentin taajuus: "))
        osoitin, kulma = laske_osoitinmuoto(input_tyyppi, input_arvo, input_taajuus)
    else:
        osoitin, kulma = laske_osoitinmuoto(input_tyyppi, input_arvo)
    print("Komponentin impedanssi osoitinmuodossa: {} < {:.3f}°"
          .format(osoitin, math.degrees(kulma)))
