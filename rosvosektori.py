import math

def laske_sektorin_ala(sade, kulma):
    sektorin_pinta_ala = kulma / 360 * math.pi * sade ** 2
    return sektorin_pinta_ala

input_sade = float(input("Anna ympyrän säde: "))
input_kulma = float(input("Anna sektorin sisäkulma asteina: "))
sektorin_ala = laske_sektorin_ala(input_sade, input_kulma)
print("Sektorin pinta-ala:", round(sektorin_ala, 4))
