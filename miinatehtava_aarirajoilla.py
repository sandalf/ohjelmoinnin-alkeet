ULKONA = -1
NURKASSA = 0
LAIDALLA = 1
KESKELLA = 2

TULOSTUKSET = {
    ULKONA: "Antamasi ruutu on kentän ulkopuolella.",
    NURKASSA: "Antamasi ruutu on kentän nurkassa.",
    LAIDALLA: "Antamasi ruutu on kentän laidalla.",
    KESKELLA: "Antamasi ruutu on keskikentällä."
}

def sijainti_kentalla(x, y, leveys, korkeus):
    if x >= leveys or x < 0 or y >= korkeus or y < 0:
        return ULKONA
    elif (x == 0 or leveys - x == 1) and (y == 0 or korkeus - y == 1):
        return NURKASSA
    elif x == 0 or leveys - x == 1 or y == 0 or korkeus - y == 1:
        return LAIDALLA
    return KESKELLA

def tulosta_sijainti(tulostukset_avain):
    print(TULOSTUKSET[tulostukset_avain])

input_leveys = int(input("Anna kentän leveys: "))
input_korkeus = int(input("Anna kentän korkeus: "))
if input_leveys <= 0 or input_korkeus <= 0:
    print()
    print("Noin pienelle kentälle ei mahdu ainuttakaan ruutua!")
else:
    input_x = int(input("Anna x-koordinaatti: "))
    input_y = int(input("Anna y-koordinaatti: "))
    sijainti = sijainti_kentalla(input_x, input_y, input_leveys, input_korkeus)
    print()
    tulosta_sijainti(sijainti)
