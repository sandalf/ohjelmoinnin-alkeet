import math

def laske_sivun_pituus(hypotenuusa):
    sivun_pituus = math.sqrt(hypotenuusa ** 2 / 2)
    return sivun_pituus

input_hypotenuusa = float(input("Anna tasakylkisen kolmion hypotenuusan pituus: "))
kateetin_pituus = laske_sivun_pituus(input_hypotenuusa)
print("Kylkien pituus:", round(kateetin_pituus, 4))
