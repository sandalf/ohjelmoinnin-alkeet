import haravasto

def piirra_kentta():
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan. Funktiota kutsutaan aina kun pelimoottori pyytää
    ruudun näkymän päivitystä.
    """
    haravasto.tyhjaa_ikkuna()
    haravasto.piirra_tausta()
    haravasto.aloita_ruutujen_piirto()
    for i, row in enumerate(planeetta):
        for j, avain in enumerate(row):
            haravasto.lisaa_piirrettava_ruutu(avain, j * 40, i * 40)
    haravasto.piirra_ruudut()

def tulvataytto(kentta, x, y):
    """
    Merkitsee planeetalla olevat tuntemattomat alueet turvalliseksi siten, että
    täyttö aloitetaan annetusta x, y -pisteestä.
    """
    if kentta[y][x] == "x":
        return
    y_sivu = len(kentta)
    x_sivu = len(kentta[0])
    ruudut = [(x, y)]
    while ruudut:
        ruutu = ruudut.pop()
        kentta[ruutu[1]][ruutu[0]] = "0"
        for i in range(max(0, ruutu[1] - 1), min(y_sivu, ruutu[1] + 2)):
            for j in range(max(0, ruutu[0] - 1), min(x_sivu, ruutu[0] + 2)):
                if kentta[i][j] == " ":
                    ruudut.append((j, i))

def main(planeetta_kentta):
    """
    Lataa pelin grafiikat, luo peli-ikkunan ja asettaa siihen piirtokäsittelijän.
    """
    korkeus = len(planeetta_kentta)
    leveys = len(planeetta_kentta[0])
    haravasto.lataa_kuvat("spritet")
    haravasto.luo_ikkuna(leveys * 40, korkeus * 40)
    haravasto.aseta_piirto_kasittelija(piirra_kentta)
    haravasto.aloita()

if __name__ == "__main__":
    planeetta = [
    [" ", " ", " ", "x", " ", " ", " ", " ", " ", " ", " ", "x", " "],
    [" ", " ", "x", "x", " ", " ", " ", "x", " ", " ", " ", "x", " "],
    [" ", "x", "x", " ", " ", " ", " ", "x", " ", " ", "x", "x", " "],
    ["x", "x", "x", "x", "x", " ", " ", "x", " ", "x", " ", " ", " "],
    ["x", "x", "x", "x", " ", " ", " ", " ", "x", " ", "x", " ", " "],
    [" ", " ", "x", " ", " ", " ", " ", " ", " ", "x", " ", " ", " "]
    ]
