def nayta_tulokset(tiedosto):
    try:
        with open(tiedosto) as lahde:
            lista = []
            for rivi in lahde.readlines():
                lista = rivi.split(",")
                print("{} {} - {} {}".format(
                    lista[0], lista[2], lista[3].strip(), lista[1]))
    except IOError:
        print("Error")
