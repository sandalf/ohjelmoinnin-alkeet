import os
import ikkunasto

tila = {
    "text": None
}

def lue_data(kansio_polku):
    """
    Lukee kaikki datatiedostot annetusta kansiosta. Ohittaa tiedostot, jotka eivät
    ole päätteeltään .mydat sekä tiedostot, joiden sisältämässä datassa on
    virheitä. Rikkinäisten tiedostojen nimet ilmoitetaan käyttäjälle.
    """
    data = []
    tiedostot = []
    sisalto = os.listdir(kansio_polku)
    for nimi in sisalto:
        print(nimi)
        if nimi.endswith(".mydat"):
            print("Good")
            polku = os.path.join(kansio_polku, nimi)
            temp_data = []
            try:
                with open(polku) as lahde:
                    for rivi in lahde.readlines():
                        if not rivi:
                            continue
                        teksti, arvo1, arvo2 = rivi.split(",")
                        temp_data.append((teksti, float(arvo1), float(arvo2)))
            except (IOError, ValueError, IndexError):
                tiedostot.append(nimi)
            else:
                data.extend(temp_data)
    return data, tiedostot


def avaa_kansio():
    """
    Napinkäsittelijä, joka pyytää käyttäjää valitsemaan kansion avaamalla
    kansioselaimen. Lataa datan valitusta kansiosta ja ilmoittaa käyttöliittymän
    tekstilaatikkoon montako riviä luettiin sekä virheellisten tiedostojen nimet.
    """
    kansio = ikkunasto.avaa_hakemistoikkuna("Kansiot")
    luettu_data, virhe_tiedostot = lue_data(kansio)
    for t_nimi in virhe_tiedostot:
        ikkunasto.kirjoita_tekstilaatikkoon(tila["text"], "Viallinen tiedosto: {}".format(t_nimi))
    ikkunasto.kirjoita_tekstilaatikkoon(tila["text"],
                                    "Luettiin {} riviä dataa.".format(len(luettu_data)))

def main():
    """
    Luo käyttöliittymäikkunan, jossa on vasemmalla kaksi nappia ja oikealla
    tekstilaatikko, johon nappia painamalla voidaan tulostaa tekstiä.
    """
    ikkuna = ikkunasto.luo_ikkuna("Nalle")
    kehys = ikkunasto.luo_kehys(ikkuna)
    ikkunasto.luo_nappi(kehys, "LATAA", avaa_kansio)
    ikkunasto.luo_nappi(kehys, "LOPETA", ikkunasto.lopeta)
    kehys = ikkunasto.luo_kehys(ikkuna)
    tila["text"] = ikkunasto.luo_tekstilaatikko(kehys)
    ikkunasto.kaynnista()

if __name__ == "__main__":
    main()
