from turtle import *

def piirra_ympyra(x, y, sade):
    up()
    setx(x)
    sety(y - sade)
    down()
    color("red")
    circle(sade)

piirra_ympyra(50, 50, 30)
up()
setx(50)
sety(50)
done()
