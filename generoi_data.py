import os
import sys
import random

NIMET = ["Muumipappa", "Hemuli", "Haisuli", "Nipsu", "Pikachu", "Bulbasaur", "Nalle Puh", "Ihaa", "Eunji", "Tomomi", "Haruna", "Seungyeon", "Ihsahn", "Abbath", "Fenriz"]

MERKIT = "abcdefghjiklmnopqrstuvwxyz"

def luo_data(kansio):
    os.makedirs(kansio, exist_ok=True)
    for i in range(10):
        with open(os.path.join(kansio, "".join([random.choice(MERKIT) for x in range(random.randint(4, 10))]) + ".mydat"), "w") as kohde:
            for i in range(random.randint(4, 20)):
                kohde.write("{},{:.4f},{:.4f}\n".format(
                    random.choice(NIMET),
                    random.random() * 1000,
                    random.random() * 100
                ))                
    
if __name__ == "__main__":
    try:
        luo_data(sys.argv[1])
    except IndexError:
        print("Ohjelman käyttö:")
        print("python generoi_data.py kansio")    