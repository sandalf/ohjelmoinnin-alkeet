def tallenna_summat(data, tiedosto):
    try:
        with open(tiedosto, "w") as kohde:
            for lista in data:
                kohde.write(":".join(lista) + "\n")
    except IOError:
        print("Error")
