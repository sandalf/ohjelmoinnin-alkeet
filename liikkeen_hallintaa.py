import math

hahmo = {
    "x": 0,
    "y": 0,
    "suunta": 0,
    "nopeus": 0
}

def muunna_xy_koordinaateiksi(kulma, vektori_pituus):
    x = vektori_pituus * math.cos(kulma)
    y = vektori_pituus * math.sin(kulma)
    return int(round(x)), int(round(y))

def kysy_liike(sanakirja):
    print("Hahmo on sijainnissa ({x}, {y})".format(x=sanakirja["x"], y=sanakirja["y"]))
    input_suunta = float(input("Anna liikkumissuunta asteina: "))
    input_nopeus = float(input("Anna liikenopeus: "))
    sanakirja["suunta"] = input_suunta
    sanakirja["nopeus"] = input_nopeus

def paivita_sijainti(sanakirja):
    suunta = math.radians(sanakirja["suunta"])
    nopeus = sanakirja["nopeus"]
    x, y = muunna_xy_koordinaateiksi(suunta, nopeus)
    sanakirja["x"] += x
    sanakirja["y"] += y
    print("Uusi sijainti: ({x}, {y})".format(x=sanakirja["x"], y=sanakirja["y"]))
