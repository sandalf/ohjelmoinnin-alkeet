import ikkunasto

tila = {
    "tekstilaatikko": None,
    "pisteet": []
}

def valitse_datapiste(mouse_event):
    """Ottaa vastaan hiiren klikkaustapahtuman ja lukee siitä datapisteen x- ja
    y-arvot. Arvot tulosteetaan tekstilaatikkoon sekä talletetaan ohjelman
    tilasanakirjassa olevaan pisteet-listaan.
    """
    x = float(mouse_event.xdata)
    y = float(mouse_event.ydata)
    tila["pisteet"].append((x, y))
    ikkunasto.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"],
                                        "Käyrän arvo pisteessä x={:.2f} on {:.2f}".format(x, y))

def main(x_data, y_data):
    """
    Luo käyttöliittymän, jossa on klikattava kuvaaja sekä tekstilaatikko. Kuvaajaan
    piirretään käyrä parametreina saatujen data-arvojen perusteella.
    """
    ikkuna = ikkunasto.luo_ikkuna("Nalle")
    kehys = ikkunasto.luo_kehys(ikkuna, ikkunasto.YLA)
    piirtoalue, kuvaaja = ikkunasto.luo_kuvaaja(kehys, valitse_datapiste, 600, 400)
    plots = kuvaaja.subplots()
    plots.plot(x_data, y_data)
    plots.set_xlim(0.0, max(x_data))
    plots.set_ylim(0.0, max(y_data))
    piirtoalue.show()
    kehys = ikkunasto.luo_kehys(ikkuna)
    tila["tekstilaatikko"] = ikkunasto.luo_tekstilaatikko(kehys)
    ikkunasto.kaynnista()

if __name__ == "__main__":
    datax = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
             1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0]
    datay = [0.0, 0.19, 0.36, 0.51, 0.64, 0.75, 0.84, 0.91, 0.96, 0.99, 1.0,
             0.99, 0.96, 0.91, 0.84, 0.75, 0.64, 0.51, 0.36, 0.19, 0.0]
    main(datax, datay)
