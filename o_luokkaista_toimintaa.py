def laske_ninjat(x, y, huonerakenne):
    """
    Laskee annetussa huoneessa yhden ruudun ympärillä olevat ninjat ja palauttaa
    niiden lukumäärän. Funktio toimii sillä oletuksella, että valitussa ruudussa ei
    ole ninjaa - jos on, sekin lasketaan mukaan.
    """
    y_sivu = len(huonerakenne)
    x_sivu = len(huonerakenne[0])
    ninjoja = 0
    for i in range(max(0, y - 1), min(y_sivu, y + 2)):
        for j in range(max(0, x - 1), min(x_sivu, x + 2)):
            if huonerakenne[i][j] == "N":
                ninjoja += 1
    return ninjoja

huone = [['N', ' ', ' ', ' ', ' '],
         ['N', 'N', 'N', 'N', ' '],
         ['N', ' ', 'N', ' ', ' '],
         ['N', 'N', 'N', ' ', ' '],
         [' ', ' ', ' ', ' ', ' '],
         [' ', ' ', ' ', ' ', ' ']]
