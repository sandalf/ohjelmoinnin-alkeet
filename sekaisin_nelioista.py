def laske_nelion_ala(sivu_pituus):
    pinta_ala = sivu_pituus ** 2
    return pinta_ala

input_pituus = float(input("Anna neliön sivun pituus: "))
nelio_ala = laske_nelion_ala(input_pituus)
print("Neliön pinta-ala:", round(nelio_ala, 4))
