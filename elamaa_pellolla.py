ELAIMET = {
    "a": "aasi",
    "k": "koira",
    "@": "kissa",
    "h": "hemuli",
    "l": "lammas"
}

def tutki_ruutu(merkki, rivi, sarake):
    """
    Funktio tutkii ruudun - jos siellä on eläin, se tulostaa eläimen sijainnin sekä nimen.
    """
    if ELAIMET.get(merkki, False):
        print("Ruudusta ({x}, {y}) löytyy {elain}".format(x=sarake, y=rivi, elain=ELAIMET[merkki]))


def tutki_kentta(kentta):
    """
    Funktio tutkii kentän sisällön käymällä sen kokonaan läpi kutsuen tutki_ruutu-funktiota
    jokaiselle kentän sisällä olevalle alkiolle.
    """
    for y, rivi in enumerate(kentta):
        for x, merkki in enumerate(rivi):
            tutki_ruutu(merkki, y, x)
