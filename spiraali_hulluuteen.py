from turtle import *

def piirra_spiraali(vari, kaari_lkm, sade, sade_kasvu, paksuus=1):
    color(vari)
    pensize(paksuus)
    begin_fill()
    for numero in range(kaari_lkm):
        circle(sade + (numero * sade_kasvu), 90)
