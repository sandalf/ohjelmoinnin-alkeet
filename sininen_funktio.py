import math

def muunna_xy_koordinaateiksi(kulma, vektori_pituus):
    x = vektori_pituus * math.cos(kulma)
    y = vektori_pituus * math.sin(kulma)
    return int(round(x)), int(round(y))
