import time
import pyglet
from pyglet.window import key
import haravasto as hv
import random as rand

VALIKKONAPIT = []
PELINLOPPUNAPIT = []
TILASTONAPIT = []
FANTASIAVALIKKONAPIT = []
TEKSTIVARI = (255, 255, 255, 255)
TEKSTILAATIKKO_VARI = (100,100,100,255)
PAAVALIKKO_LEVEYS = 400
PAAVALIKKO_KORKEUS = 700
TILASTOT_LEVEYS = 600
STATISTIIKKA_KORKEUS = 150
STATISTIIKKA_LEVEYS = 300
FANTASIAVALIKKO_KOKO = 400
FONTTI = "serif"

FANTASIAVALIKKO = {
    "leveys": 0,
    "korkeus": 0,
    "miina_lkm": 0,
    "virheviesti": "",
    "muokattava_avain": None
}

TILASTOT = {
    "siirrot": [],
    "aika": [],
    "pelimoodi": [],
    "havitty_peli": []
}

PELIMOODIT = {
    0: (8, 8, 10),
    1: (16, 16, 40),
    2: (24, 24, 99),
    3: (24, 24, 575)
}

PELIMOODI_NIMET = {
    0: "Helppo",
    1: "Keskitaso",
    2: "Vaikea",
    3: "JOPO",
    4: "Fantasia"
}

tila = {
    "kentta": None,
    "leveys": None,
    "korkeus": None,
    "miina_lkm": None,
    "siirrot": None,
    "aika": None,
    "liput": None,
    "havitty_peli": None,
    "pelimoodi": None
}

def kentta_valmistelut(leveys, korkeus, miina_lkm):
    """
    Luo, miinoittaa ja lataa kentän. Päivittää kentän tiedot sanakirjaan ja asettaa pelin
    hiirikäsittelijän ja toistuvan käsittelijän.
    """
    luo_kentta(leveys, korkeus)
    miinoita_kentta(leveys, korkeus, miina_lkm)
    lataa_kentta(leveys, korkeus)
    tila["siirrot"] = 0
    tila["aika"] = 0
    tila["miina_lkm"] = miina_lkm
    tila["liput"] = miina_lkm
    tila["havitty_peli"] = False
    hv.aseta_toistuva_kasittelija(sekuntikello_kasittelija, 1)
    hv.aseta_hiiri_kasittelija(peli_kasittelija)

def luo_kentta(leveys, korkeus):
    """
    Luo kentän eli listan jossa on korkeuden määrän alkioita. Listan alkiot ovat listoja
    joissa on leveyden määrän alkioita joiden arvoksi asetetaan " ". Päivittää kentän ja
    sen leveyden ja korkeuden sanakirjaan.
    """
    kentta = []
    for rivi in range(korkeus):
        kentta.append([])
        for sarake in range(leveys):
            kentta[-1].append(" ")
    tila["kentta"] = kentta
    tila["leveys"] = leveys
    tila["korkeus"] = korkeus

def miinoita_kentta(leveys, korkeus, miina_lkm):
    """
    Miinoittaa kentän valitsemalla satunnaisesti ruudun x ja y arvon. Valittu ruutu
    tallennetaan settiin. Tätä jatketaan kunnes on valittu miina_lkm määrän ruutuja.
    Lopuksi kentän ruutuihin asetetaan miina, merkkinä käytetään "x ".
    """
    miinaruudut = set([])
    kentta = tila["kentta"]
    while len(miinaruudut) < miina_lkm:
        miinaruudut.add((rand.randint(0, leveys - 1), rand.randint(0, korkeus - 1)))
    for ruutu in miinaruudut:
        kentta[ruutu[1]][ruutu[0]] = "x "

def lataa_kentta(leveys, korkeus):
    """
    Lataa pelin grafiikat, asettaa peli-ikkunan koon ja asettaa siihen piirtokäsittelijän.
    """
    hv.lataa_kuvat("spritet")
    hv.muuta_ikkunan_koko(max(leveys * 40, STATISTIIKKA_LEVEYS),
                          korkeus * 40 + STATISTIIKKA_KORKEUS)
    hv.aseta_piirto_kasittelija(piirra_kentta)

def piirra_kentta():
    """
    Käsittelijäfunktio, joka piirtää kaksiulotteisena listana kuvatun miinakentän
    ruudut näkyviin peli-ikkunaan ja sen yläpuolella tietorivin jossa näkyy lippujen
    määrä ja kulunut aika. Funktiota kutsutaan aina kun pelimoottori pyytää ruudun
    näkymän päivitystä.
    """
    hv.tyhjaa_ikkuna()
    hv.piirra_tausta()
    hv.aloita_ruutujen_piirto()
    kentta = tila["kentta"]
    if tila["havitty_peli"]:
        indeksi = 0
    else:
        indeksi = -1
    for i, row in enumerate(kentta):
        for j, avain in enumerate(row):
            if avain == "x":
                avain = "0"
            hv.lisaa_piirrettava_ruutu(avain[indeksi], j * 40, i * 40)
    hv.lisaa_piirrettava_ruutu("f", 10, tila["korkeus"] * 40 + 10)
    hv.piirra_ruudut()
    hv.piirra_tekstia("{:03}".format(tila["liput"]), 65, tila["korkeus"] * 40 + 5)
    hv.piirra_tekstia("{:03}".format(tila["aika"]),
                      max(tila["leveys"] * 40, STATISTIIKKA_LEVEYS) - 100,
                      tila["korkeus"] * 40 + 5)

def tutki_ruutu(x, y):
    """
    Tutkii kuinka monta miinaa on annetuja koordinaatteja vastaavaan ruudun viereisissä ruuduissa.
    Poistaa myös ruudun mahdollisen liputuksen. Päivittää saadun numeron sanakirjassa olevaan
    kenttään ja palauttaa saadun numeron tekstijonona.
    """
    y_sivu = tila["korkeus"]
    x_sivu = tila["leveys"]
    kentta = tila["kentta"]
    liputa_ruutu(x, y, True)
    miinoja = 0
    for i in range(max(0, y - 1), min(y_sivu, y + 2)):
        for j in range(max(0, x - 1), min(x_sivu, x + 2)):
            if kentta[i][j][0] == "x":
                miinoja += 1
    kentta[y][x] = str(miinoja)
    return str(miinoja)

def tulvataytto(x, y):
    """
    Merkitsee kentässä olevat tuntemattomat ruudut nollaksi siten, että
    täyttö aloitetaan annetusta x, y -pisteestä.
    """
    kentta = tila["kentta"]
    y_sivu = len(kentta)
    x_sivu = len(kentta[0])
    ruudut = [(x, y)]
    while ruudut:
        ruutu = ruudut.pop()
        for i in range(max(0, ruutu[1] - 1), min(y_sivu, ruutu[1] + 2)):
            for j in range(max(0, ruutu[0] - 1), min(x_sivu, ruutu[0] + 2)):
                if kentta[i][j][0] == " ":
                    tutkittu_ruutu = tutki_ruutu(j, i)
                    if tutkittu_ruutu == "0":
                        ruudut.append((j, i))

def liputa_ruutu(x, y, vain_poisto=False):
    """
    Liputtaa tai poistaa liputuksen riippuen onko ruudussa lippu vai ei.
    """
    if tila["kentta"][y][x][-1] == "f":
        tila["kentta"][y][x] = tila["kentta"][y][x][:-1]
        tila["liput"] += 1
    elif not vain_poisto:
        tila["kentta"][y][x] += "f"
        tila["liput"] -= 1

def tarkista_voittoehto():
    """
    Tutkii onko peli voitettu käymällä läpi kentän kaikki ruudut etsien tyhjää ruutua,
    jos tyhjä ruutu löytyy palauttaa False. Muuten peli on voitettu ja palauttaa True.
    """
    for rivi in tila["kentta"]:
        for ruutu in rivi:
            if ruutu[0] == " ":
                return False
    return True

def luo_valikkonappi(teksti, leveys, korkeus, y=0, x=-1, ikkunan_leveys=PAAVALIKKO_LEVEYS,
                     taustavari=TEKSTILAATIKKO_VARI, tekstivari=TEKSTIVARI):
    """
    Luo "napin" luomalla taustan jonka mitat ovat leveys ja korkeus ja asettaa sen koordinaatteihin
    x ja y. Jos x on negatiivinen, asetetaan nappi ikkunan keskelle. Asettaa taustan päälle halutun
    tekstin. Palauttaa tausta- ja tekstiobjektin.
    """
    if x < 0:
        x=ikkunan_leveys / 2 - leveys / 2
    nappi_tausta = pyglet.sprite.Sprite(pyglet.image.SolidColorImagePattern(taustavari)
                                        .create_image(leveys, korkeus), x=x, y=y)
    nappi_teksti = pyglet.text.Label(teksti, font_name=FONTTI, font_size=32,
                                      color=tekstivari, anchor_x="left", anchor_y="bottom")
    nappi_teksti.x = nappi_tausta.x + (leveys / 2 - nappi_teksti.content_width / 2)
    nappi_teksti.y = y + (korkeus / 2 - nappi_teksti.content_height / 2)
    return nappi_tausta, nappi_teksti

def kaynnista_siirtymavaihe():
    """
    Käynnistää siirtymävaiheen eri valikoiden tai pelimuotojen välillä. Asettaa kaikki käsittelijät
    tyhjiksi, tyhjentää ikkunan ja tyhjentää kaikki valikkonappilistat.
    """
    hv.aseta_hiiri_kasittelija(tyhja_hiiri_kasittelija)
    hv.aseta_piirto_kasittelija(tyhja_piirto_kasittelija)
    hv.aseta_nappain_kasittelija(tyhja_nappain_kasittelija)
    hv.tyhjaa_ikkuna()
    VALIKKONAPIT.clear()
    PELINLOPPUNAPIT.clear()
    TILASTONAPIT.clear()
    FANTASIAVALIKKONAPIT.clear()

def piirra_pelinloppuvalikko():
    """
    Pelinlopun piirtokäsittelijän apufunktio joka piirtää valikkonapit.
    """
    for tausta, teksti in PELINLOPPUNAPIT:
        tausta.draw()
        teksti.draw()

def pysayta_sekuntikello(kasittelija):
    """
    Pysäyttää Haravaston käynnistämän toistuvan käsittelijän. Funktio joka mielestäni pitäisi
    löytyä Haravastosta natiivina.
    """
    pyglet.clock.unschedule(kasittelija)

def valikkonappi_kasittelija(indeksi):
    """
    Päävalikon hiirikäsittelijän apufunktio joka ottaa painetun napin indeksin ja käynnistää
    oikean pelimoodin, avaa tilastavalikon tai lopettaa ohjelman.
    """
    if indeksi == 4:
        luo_fantasiavalikko()
    elif indeksi == 5:
        luo_tilastovalikko()
    elif indeksi == 6:
        hv.lopeta()
    else:
        aloita_peli(indeksi)

def fantasiavalikko_tekstinsyotto(indeksi):
    """
    Fantasiavalikon hiirikäsittelijän apufunktio joka mahdollistaa tekstinsyötön. Ottaa muokattavan
    tekstikentän indeksin, asettaa sen tekstikentän avaimen sanakirjaan muokattavaksi avaimeksi,
    nollaa tekstikentän ja asettaa ikkunaan näppäinkäsittelijän.
    """
    FANTASIAVALIKKO["muokattava_avain"] = list(FANTASIAVALIKKO)[indeksi]
    FANTASIAVALIKKO[FANTASIAVALIKKO["muokattava_avain"]] = ""
    hv.aseta_nappain_kasittelija(fantasiavalikko_nappainkasittelija)

def aloita_peli(pelimoodi):
    """
    Hakee pelimoodin perusteella sanakirjasta kentän leveyden, korkeuden ja miinojen lukumäärän,
    antaa ne kentta_valmisteluun argumentteina ja päivittää pelimoodin numeron sanakirjaan.
    """
    peli = PELIMOODIT[pelimoodi]
    tila["pelimoodi"] = pelimoodi
    kentta_valmistelut(peli[0], peli[1], peli[2])

def lopeta_peli(havitty_peli):
    """
    Lopettaa käynnissä olevan pelin, pysäyttää pelikellon, päivittää tuloksen sanakirjaan,
    tallentaa tilastot, luo pelinlopun valikon ja asettaa sen hiiri- ja piirtokäsittelijän.
    """
    pysayta_sekuntikello(sekuntikello_kasittelija)
    tila["havitty_peli"] = havitty_peli
    tallenna_tilastot()
    hv.aseta_hiiri_kasittelija(pelinloppu_kasittelija)
    luo_pelinloppuvalikko()
    hv.aseta_piirto_kasittelija(pelinloppu_piirtokasittelija)

def tallenna_tilastot():
    """
    Tallentaa tilastot CSV-formaatissa tilastot.txt tiedostoon.
    """
    try:
        with open("tilastot.txt", "a") as kohde:
            kohde.write("{},{},{},{}\n".format(tila["siirrot"], tila["aika"],
                        tila["havitty_peli"], tila["pelimoodi"]))
    except IOError:
        print("Kohdetiedostoa ei voitu avata. Tallennus epäonnistui")

def lataa_tilastot():
    """
    Lataa tilastot tilastot.txt tiedostosta ja tallentaa ne sanakirjaan takaperin käännettyinä
    listoina.
    """
    TILASTOT["siirrot"].clear()
    TILASTOT["aika"].clear()
    TILASTOT["pelimoodi"].clear()
    TILASTOT["havitty_peli"].clear()
    try:
        with open("tilastot.txt") as lahde:
            lista = []
            for rivi in lahde.readlines():
                lista = rivi.split(",")
                TILASTOT["siirrot"].append(int(lista[0]))
                TILASTOT["aika"].append(int(lista[1]))
                TILASTOT["pelimoodi"].append(int(lista[3]))
                if lista[2] == "True":
                    TILASTOT["havitty_peli"].append(True)
                elif lista[2] == "False":
                    TILASTOT["havitty_peli"].append(False)
                else:
                    raise IOError
    except IOError:
        print("Kohdetiedostoa ei voitu avata. Luku epäonnistui")
        TILASTOT["siirrot"].clear()
        TILASTOT["aika"].clear()
        TILASTOT["pelimoodi"].clear()
        TILASTOT["havitty_peli"].clear()
    TILASTOT["siirrot"].reverse()
    TILASTOT["aika"].reverse()
    TILASTOT["pelimoodi"].reverse()
    TILASTOT["havitty_peli"].reverse()

def sekuntikello_kasittelija(kulunut_aika):
    """
    Toistuvakäsittelijä joka päivittää pelikelloa sanakirjaan sekunnin välein.
    """
    tila["aika"] += 1

def piirra_keskiarvotilastot(y):
    """
    Tilastojen piirtokäsittelijän apufunktio joka laskee tilastojen keskiarvot ja piirtää ne
    ikkunaan. Tilastot piirretään allekkain.
    """
    keskisiirrot = int(sum(TILASTOT["siirrot"]) / len(TILASTOT["siirrot"]) + 0.5)
    keskiaika = int(sum(TILASTOT["aika"]) / len(TILASTOT["aika"]) + 0.5)
    lempivaikeustaso = int(sum(TILASTOT["pelimoodi"]) / len(TILASTOT["pelimoodi"]) + 0.5)
    voitot = TILASTOT["havitty_peli"].count(False)
    pelatut_pelit = len(TILASTOT["havitty_peli"])
    hv.piirra_tekstia("Siirtojen keskiarvo", 10, y - 50)
    hv.piirra_tekstia("Keston keskiarvo", 10, y - 100)
    hv.piirra_tekstia("Suosituin peli", 10, y - 150)
    hv.piirra_tekstia("Pelattuja pelejä", 10, y - 200)
    hv.piirra_tekstia("Voittoja", 10, y - 250)
    hv.piirra_tekstia(str(keskisiirrot), 500, y - 50)
    hv.piirra_tekstia(str(keskiaika), 500, y - 100)
    hv.piirra_tekstia(PELIMOODI_NIMET[lempivaikeustaso], 410, y - 150)
    hv.piirra_tekstia(str(pelatut_pelit), 500, y - 200)
    hv.piirra_tekstia(str(voitot), 500, y - 250)

def piirra_pelitilastot(y):
    """
    Tilastojen piirtokäsittelijän apufunktio joka piirtää enintään viimeisimmän kymmenen pelin
    tilastot ikkunaan. Tilastot piirretään allekkain.
    """
    hv.piirra_tekstia("Viimeiset 10 peliä", 10, y)
    y -= 45
    hv.piirra_tekstia("Siirrot", 10, y)
    hv.piirra_tekstia("Aika", 130, y)
    hv.piirra_tekstia("Tulos", 220, y)
    hv.piirra_tekstia("Pelimoodi", 340, y)
    y -= 45
    for i in range(0, min(len(TILASTOT["siirrot"]), 10)):
        hv.piirra_tekstia(str(TILASTOT["siirrot"][i]), 30, y - i * 45)
        hv.piirra_tekstia(str(TILASTOT["aika"][i]), 135, y - i * 45)
        if TILASTOT["havitty_peli"][i]:
            teksti = "Häviö"
        else:
            teksti = "Voitto"
        hv.piirra_tekstia(teksti, 220, y - i * 45)
        hv.piirra_tekstia(PELIMOODI_NIMET[TILASTOT["pelimoodi"][i]], 350, y - i * 45)

def kaynnista_paavalikko():
    """
    Käynnistää päävalikon asettamalla ikkunan koon, luomalla päävalikon napit ja asettamalla
    sen hiiri- ja piirtokäsittelijät.
    """
    hv.muuta_ikkunan_koko(PAAVALIKKO_LEVEYS, PAAVALIKKO_KORKEUS)
    luo_paavalikko()
    hv.aseta_hiiri_kasittelija(paavalikko_kasittelija)
    hv.aseta_piirto_kasittelija(paavalikko_piirtokasittelija)

def luo_paavalikko():
    """
    Luo päävalikon napit ja tallentaa ne listaan
    """
    VALIKKONAPIT.append(luo_valikkonappi("Helppo", 200, 50, 630))
    VALIKKONAPIT.append(luo_valikkonappi("Keskitaso", 200, 50, 530))
    VALIKKONAPIT.append(luo_valikkonappi("Vaikea", 200, 50, 430))
    VALIKKONAPIT.append(luo_valikkonappi("JOPO", 200, 50, 330))
    VALIKKONAPIT.append(luo_valikkonappi("Fantasia", 200, 50, 230))
    VALIKKONAPIT.append(luo_valikkonappi("Tilastot", 200, 50, 130))
    VALIKKONAPIT.append(luo_valikkonappi("Lopeta", 200, 50, 30))

def luo_pelinloppuvalikko():
    """
    Luo popup tyylisen valikon joka näytetään kun peli päättyy. Valikossa on pelin lopputulos
    ja kaksi nappia joilla voi aloittaa uuden pelin tai palata takaisin päävalikkoon.
    """
    if tila["havitty_peli"]:
        teksti = "Hävisit pelin"
        miinus_x = 125
    else:
        teksti = "Voitit pelin"
        miinus_x = 125
    keskita_x = max(tila["leveys"] * 40, STATISTIIKKA_LEVEYS) / 2
    keskita_y = max(tila["korkeus"] * 40, STATISTIIKKA_KORKEUS) - 20
    PELINLOPPUNAPIT.append(luo_valikkonappi(teksti, 250, 50, keskita_y, keskita_x - miinus_x))
    PELINLOPPUNAPIT.append(luo_valikkonappi("Uusi peli", 200, 50, keskita_y - 60, keskita_x - 100))
    PELINLOPPUNAPIT.append(luo_valikkonappi("Poistu", 200, 50, keskita_y - 120, keskita_x - 100))

def luo_tilastovalikko():
    """
    Lataa tilastot, muuttaa ikkunan koon, luo kaksi nappia joilla voi palata päävalikkoon
    tai tyhjentää tilastot ja asettaa ikkunan hiiri- ja piirtokäsittelijän.
    """
    lataa_tilastot()
    korkeus = max(min(len(TILASTOT["aika"]), 10) * 45 + 430, 300)
    hv.muuta_ikkunan_koko(TILASTOT_LEVEYS, korkeus)
    TILASTONAPIT.append(luo_valikkonappi("Palaa", 120, 50, korkeus - 60, 10))
    TILASTONAPIT.append(luo_valikkonappi("Tyhjennä tilastot", 350, 50, korkeus - 60, 180))
    hv.aseta_hiiri_kasittelija(tilastovalikko_kasittelija)
    hv.aseta_piirto_kasittelija(tilastovalikko_piirtokasittelija)

def luo_fantasiavalikko():
    """
    Luo valikon jolla voi asettaa itse kentän koon ja miinojen lukumäärän. Asettaa ikkunan koon,
    lue tekstikentät ja 2 nappia joilla voi palata päävalikkoon tai aloittaa pelin, nollaa
    sanakirjan virheviestin ja muokattavan avaimen ja asettaa ikkunan hiiri- ja piirtokäsittelijän.
    """
    hv.muuta_ikkunan_koko(FANTASIAVALIKKO_KOKO, FANTASIAVALIKKO_KOKO)
    FANTASIAVALIKKO["virheviesti"] = ""
    FANTASIAVALIKKO["muokattava_avain"] = ""
    FANTASIAVALIKKONAPIT.append(luo_valikkonappi("", 90, 50, FANTASIAVALIKKO_KOKO - 120, 200))
    FANTASIAVALIKKONAPIT.append(luo_valikkonappi("", 90, 50, FANTASIAVALIKKO_KOKO - 200, 200))
    FANTASIAVALIKKONAPIT.append(luo_valikkonappi("", 90, 50, FANTASIAVALIKKO_KOKO - 280, 200))
    FANTASIAVALIKKONAPIT.append(luo_valikkonappi("Palaa", 150, 50, 10, 10))
    FANTASIAVALIKKONAPIT.append(luo_valikkonappi("Aloita", 150, 50, 10, 240))
    hv.aseta_hiiri_kasittelija(fantasiavalikko_kasittelija)
    hv.aseta_piirto_kasittelija(fantasiavalikko_piirtokasittelija)

def tyhja_hiiri_kasittelija(x, y, nappi, muokkaus):
    """
    Tyhjä hiirikäsittelijä jota käytetään siirtymisvaiheessa.
    """
    pass

def paavalikko_kasittelija(x, y, nappi, muokkaus):
    """
    Selvittää osuiko hiiren painallus nappiin ja ja mihin niistä. Käynnistää siirtymävaiheen ja
    antaa painetun napin indeksin apufunktiolle.
    """
    for indeksi, tausta in enumerate(VALIKKONAPIT):
        if tausta[0].y <= y <= (tausta[0].y + tausta[0].height):
            if tausta[0].x <= x <= (tausta[0].x + tausta[0].width):
                kaynnista_siirtymavaihe()
                valikkonappi_kasittelija(indeksi)

def peli_kasittelija(x, y, nappi, muokkaus):
    """
    Laskee mitä ruutua painettiin. Jos käytetty painike oli hiiren oikea painike, tarkistaa onko
    ettei ruudussa ole numeroa ja liputtaa sen. Vasemmalla painikkeella tarkistaa onko ruudussa
    miina, tällöin käynnistää siirtymävaiheen ja lopettaa pelin. Jos ruutu on tyhjä, se
    tarkistetaan ja tarpeen tullen käytetään tulvatäyttöä. Tämän jälkeen tarkistetaan vielä
    täyttyikö voittoehto ja tarpeen tullen lopetataan peli.
    """
    ruutu_x = x // 40
    ruutu_y = y // 40
    ruutu = tila["kentta"][ruutu_y][ruutu_x]
    if nappi == hv.HIIRI_VASEN:
        if ruutu[0] == "x":
            tila["siirrot"] += 1
            kaynnista_siirtymavaihe()
            lopeta_peli(True)
        elif ruutu[0] == " ":
            tila["siirrot"] += 1
            if tutki_ruutu(ruutu_x, ruutu_y) == "0":
                tulvataytto(ruutu_x, ruutu_y)
            if tarkista_voittoehto():
                kaynnista_siirtymavaihe()
                lopeta_peli(False)
    elif nappi == hv.HIIRI_OIKEA and (ruutu[0] == " " or ruutu[0] == "x"):
        tila["siirrot"] += 1
        liputa_ruutu(ruutu_x, ruutu_y)

def pelinloppu_kasittelija(x, y, nappi, muokkaus):
    """
    Selvittää osuiko hiiren painallus nappiin ja ja mihin niistä. Käynnistää siirtymävaiheen ja
    käynnistää uuden pelin tai palaa päävalikkoon.
    """
    for indeksi, tausta in enumerate(PELINLOPPUNAPIT):
        if tausta[0].y <= y <= (tausta[0].y + tausta[0].height):
            if tausta[0].x <= x <= (tausta[0].x + tausta[0].width):
                if indeksi == 1:
                    kaynnista_siirtymavaihe()
                    kentta_valmistelut(tila["leveys"], tila["korkeus"], tila["miina_lkm"])
                elif indeksi == 2:
                    kaynnista_siirtymavaihe()
                    kaynnista_paavalikko()

def tilastovalikko_kasittelija(x, y, nappi, muokkaus):
    """
    Selvittää osuiko hiiren painallus nappiin ja ja mihin niistä. Käynnistää siirtymävaiheen ja
    tyhjentää tilastot tai palaa päävalikkoon.
    """
    for indeksi, tausta in enumerate(TILASTONAPIT):
        if tausta[0].y <= y <= (tausta[0].y + tausta[0].height):
            if tausta[0].x <= x <= (tausta[0].x + tausta[0].width):
                if indeksi == 0:
                    kaynnista_siirtymavaihe()
                    kaynnista_paavalikko()
                else:
                    with open("tilastot.txt", 'w'):
                        pass
                    kaynnista_siirtymavaihe()
                    luo_tilastovalikko()

def fantasiavalikko_kasittelija(x, y, nappi, muokkaus):
    """
    Selvittää osuiko hiiren painallus nappiin ja ja mihin niistä. Käynnistää siirtymävaiheen ja
    palaa päävalikkoon, aloittaa uuden pelin jos annetut arvot ovat hyväksyttyjä tai käynnistää
    tekstinsyötön apufunktion.
    """
    for indeksi, tausta in enumerate(FANTASIAVALIKKONAPIT):
        if tausta[0].y <= y <= (tausta[0].y + tausta[0].height):
            if tausta[0].x <= x <= (tausta[0].x + tausta[0].width):
                if indeksi == 3:
                    kaynnista_siirtymavaihe()
                    kaynnista_paavalikko()
                elif indeksi == 4:
                    try:
                        leveys = int(FANTASIAVALIKKO["leveys"])
                        korkeus = int(FANTASIAVALIKKO["korkeus"])
                        miina_lkm = int(FANTASIAVALIKKO["miina_lkm"])
                    except ValueError:
                        FANTASIAVALIKKO["virheviesti"] = "Pyydettyjen arvojen tulee olla numeroita"
                    else:
                        if leveys == 0 or korkeus == 0 or miina_lkm == 0:
                            FANTASIAVALIKKO["virheviesti"] = "Pyydetyt arvot eivät saa olla 0"
                        elif leveys * korkeus <= miina_lkm:
                            FANTASIAVALIKKO["virheviesti"] = "Miinoja ei voi olla enemmän kuin ruutuja"
                        elif leveys > 25 or korkeus > 25:
                            FANTASIAVALIKKO["virheviesti"] = "Maksimikoko on 25x25"
                        else:
                            kaynnista_siirtymavaihe()
                            tila["pelimoodi"] = 4
                            kentta_valmistelut(leveys, korkeus, miina_lkm)
                else:
                    fantasiavalikko_tekstinsyotto(indeksi)

def tyhja_piirto_kasittelija():
    """
    Tyhjä piirtokäsittelijä jota käytetään siirtymisvaiheessa.
    """
    pass

def paavalikko_piirtokasittelija():
    """
    Piirtää ikkunaan päävalikon napit.
    """
    hv.tyhjaa_ikkuna()
    hv.piirra_tausta()
    for tausta, teksti in VALIKKONAPIT:
        tausta.draw()
        teksti.draw()

def pelinloppu_piirtokasittelija():
    """
    Piirtää popup tyylisen pelinlopun valikon apufunktioiden avulla.
    """
    piirra_kentta()
    piirra_pelinloppuvalikko()

def tilastovalikko_piirtokasittelija():
    """
    Piirtää tilastonapit ikkunaan ja piirtää tilastot apufunktioiden avulla. Jos tilastoja
    ei ole, piirtää siitä ilmoittavan tekstin.
    """
    hv.tyhjaa_ikkuna()
    hv.piirra_tausta()
    for tausta, teksti in TILASTONAPIT:
        tausta.draw()
        teksti.draw()
    if len(TILASTOT["siirrot"]) == 0:
        hv.piirra_tekstia("Ei tilastoja", 10, tausta.y - 50)
    else:
        piirra_keskiarvotilastot(tausta.y)
        piirra_pelitilastot(tausta.y - 320)

def fantasiavalikko_piirtokasittelija():
    """
    Piirtää fantasiavalikon jossa käyttäjä voi itse antaa haluamansa kentän koon ja miinojen
    lukumäärän.
    """
    hv.tyhjaa_ikkuna()
    hv.piirra_tausta()
    hv.piirra_tekstia("Anna peliarvot", 80, FANTASIAVALIKKO_KOKO - 50)
    hv.piirra_tekstia("Leveys", 10, FANTASIAVALIKKO_KOKO - 120)
    hv.piirra_tekstia("Korkeus", 10, FANTASIAVALIKKO_KOKO - 200)
    hv.piirra_tekstia("Miinoja", 10, FANTASIAVALIKKO_KOKO - 280)
    for tausta, teksti in FANTASIAVALIKKONAPIT:
        tausta.draw()
        teksti.draw()
    hv.piirra_tekstia(str(FANTASIAVALIKKO["leveys"]), 200, FANTASIAVALIKKO_KOKO - 120)
    hv.piirra_tekstia(str(FANTASIAVALIKKO["korkeus"]), 200, FANTASIAVALIKKO_KOKO - 200)
    hv.piirra_tekstia(str(FANTASIAVALIKKO["miina_lkm"]), 200, FANTASIAVALIKKO_KOKO - 280)
    hv.piirra_tekstia(FANTASIAVALIKKO["virheviesti"], 10, 70, vari=(255, 0, 0, 255), koko=16)

def tyhja_nappain_kasittelija():
    """
    Tyhjä näppäinkäsittelijä jota käytetään siirtymisvaiheessa.
    """
    pass

def fantasiavalikko_nappainkasittelija(s, muokkaus):
    """
    Fantasiavalikon näppäinkäsittelijä jonka avulla voi syöttää numeroita valittuun
    tekstikenttään. Tallentaa syötetyn tekstin sanakirjaan
    """
    teksti = FANTASIAVALIKKO[FANTASIAVALIKKO["muokattava_avain"]]
    if len(teksti) >= 3 and not s == key.BACKSPACE:
        return
    elif s == key.BACKSPACE and len(teksti) > 0:
        teksti = teksti[:-1]
    elif s == key._0 or s == key.NUM_0:
        teksti += "0"
    elif s == key._1 or s == key.NUM_1:
        teksti += "1"
    elif s == key._2 or s == key.NUM_2:
        teksti += "2"
    elif s == key._3 or s == key.NUM_3:
        teksti += "3"
    elif s == key._4 or s == key.NUM_4:
        teksti += "4"
    elif s == key._5 or s == key.NUM_5:
        teksti += "5"
    elif s == key._6 or s == key.NUM_6:
        teksti += "6"
    elif s == key._7 or s == key.NUM_7:
        teksti += "7"
    elif s == key._8 or s == key.NUM_8:
        teksti += "8"
    elif s == key._9 or s == key.NUM_9:
        teksti += "9"
    FANTASIAVALIKKO[FANTASIAVALIKKO["muokattava_avain"]] = teksti

if __name__ == "__main__":
    hv.luo_ikkuna(PAAVALIKKO_LEVEYS, PAAVALIKKO_KORKEUS)
    kaynnista_paavalikko()
    hv.aloita()
