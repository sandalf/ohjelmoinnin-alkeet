def laske_nopeus(matka, aika):
    nopeus = matka / aika * 3.6
    return nopeus

try:
    input_matka = float(input("Anna auton kulkema matka (m): "))
    input_aika = float(input("Anna matkaan kulunut aika (s): "))
except ValueError:
    print("Vähemmän donitseja, enemmän puhtaita numeroita.")
else:
    vauhti = laske_nopeus(input_matka, input_aika)
    print("{matka:.2f} metriä {aika:.2f} sekunnissa kulkeneen auton nopeus on {nopeus:.2f} km/h."
          .format(matka=input_matka, aika=input_aika, nopeus=vauhti))
