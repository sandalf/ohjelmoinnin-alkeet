import ikkunasto

elementit = {
    "tekstilaatikko": None
}


def tulosta_testirivi():
    """
    Tulostaa testirivin annettuun tekstilaatikkoon.
    """
    ikkunasto.kirjoita_tekstilaatikkoon(elementit["tekstilaatikko"], "aasisvengaa")

def main():
    """
    Luo käyttöliittymäikkunan, jossa on vasemmalla kaksi nappia ja oikealla
    tekstilaatikko, johon nappia painamalla voidaan tulostaa tekstiä.
    """
    ikkuna = ikkunasto.luo_ikkuna("Nalle")
    kehys = ikkunasto.luo_kehys(ikkuna)
    ikkunasto.luo_nappi(kehys, "PAINA", tulosta_testirivi)
    ikkunasto.luo_nappi(kehys, "LOPETA", ikkunasto.lopeta)
    kehys = ikkunasto.luo_kehys(ikkuna)
    elementit["tekstilaatikko"] = ikkunasto.luo_tekstilaatikko(kehys)
    ikkunasto.kaynnista()

if __name__ == "__main__":
    main()
