def laske_parametrit(x1, y1, x2, y2):
    kkerroin = (y2 - y1) / (x2 - x1)
    vtermi = (x2 * y1 - x1 * y2) / (x2 - x1)
    return kkerroin, vtermi

def kysy_kaksi_lukua(input_str):
    """
    Pyytää käyttäjältä kaksi liukulukua välilyönnillä erotettuna. Syötettä kysytään
    kunnes käyttäjä antaa kaksi kelvollista liukulukua. Syötetyt luvut palautetaan
    liukulukuina.
    """
    while True:
        input_numerot = input(input_str).strip()
        try:
            input_x, input_y = input_numerot.split(" ")
            input_x, input_y = float(input_x), float(input_y)
        except ValueError:
            print("Anna kaksi lukua välilyönnillä erotettuna")
        else:
            return input_x, input_y

def laske_pisteet_suoralla(kkerroin, vtermi, pisteet):
    """
    Tuottaa joukon pisteitä, jotka ovat annetulla kulmakertoimella ja vakiotermillä
    määritetyn suoran arvoja annetuissa x-akselin pisteissä.
    """
    y_pisteet = []
    for x in pisteet:
        y_pisteet.append(kkerroin * x + vtermi)
    return y_pisteet

x_akseli = [0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5,
            3.75, 4.0, 4.25, 4.5, 4.75, 5.0]
x_1, y_1 = kysy_kaksi_lukua("Anna ensimmäinen piste: ")
x_2, y_2 = kysy_kaksi_lukua("Anna toinen piste: ")
kulmakerroin, vakiotermi = laske_parametrit(x_1, y_1, x_2, y_2)
y_akseli = laske_pisteet_suoralla(kulmakerroin, vakiotermi, x_akseli)
for y in y_akseli:
    print("{:.4f}".format(y), end=" ")
