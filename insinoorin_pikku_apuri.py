YKSIKOT = {
    "Y": 10 ** 24,
    "Z": 10 ** 21,
    "E": 10 ** 18,
    "P": 10 ** 15,
    "T": 10 ** 12,
    "G": 10 ** 9,
    "M": 10 ** 6,
    "k": 10 ** 3,
    "h": 10 ** 2,
    "da": 10,
    "d": 10 ** -1,
    "c": 10 ** -2,
    "m": 10 ** -3,
    "u": 10 ** -6,
    "n": 10 ** -9,
    "p": 10 ** -12,
    "f": 10 ** -15,
    "a": 10 ** -18,
    "z": 10 ** -21,
    "y": 10 ** -24
}

def muuta_kerrannaisyksikko(luku):
    """
    Muuttaa annetun luvun ja mahdollisen kerrannaisyksikön vastaavaksi liukuluvuksi.
    """
    if luku.isnumeric():
        return float(luku)
    elif luku.isalpha():
        return None
    numero = luku[:-1]
    yksikko = luku[-1:]
    if YKSIKOT.get(yksikko, False):
        return float(numero) * YKSIKOT[yksikko]
    return None

while True:
    arvo = muuta_kerrannaisyksikko(input("Anna muutettava arvo: ").strip())
    if arvo is None:
        print("Arvo ei ole kelvollinen")
    else:
        print(arvo)
        break
