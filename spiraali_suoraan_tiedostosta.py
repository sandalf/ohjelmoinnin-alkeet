from turtle import *

def piirra_spiraali(vari, kaari_lkm, sade, sade_kasvu, paksuus=1):
    color(vari)
    pensize(paksuus)
    begin_fill()
    for numero in range(kaari_lkm):
        circle(sade + (numero * sade_kasvu), 90)

def piirra_tiedostosta(tiedosto):
    try:
        with open(tiedosto) as lahde:
            lista = []
            for rivi in lahde.readlines():
                lista = rivi.split(",")
                piirra_spiraali(lista[0], int(lista[1]), int(lista[2]),
                                float(lista[3]), int(lista[4]))
    except IOError:
        print("Error")
